#! /usr/bin/python
# -*- coding: utf-8 -*-

import datetime
import logging
import socket

class RaspClient:
  # TODO: Should increase this when neccesary??
  buffer_size = 1024

  def __init__(self, host, port):
    self.server_address = (host, port)

    try:
      self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    except socket.error, msg : 
      self.sock = None
      logging.info('Failed to create socket: ' + msg)

  def valid(self):
    return self.sock != None

  def send(self, data):
    try:
      send_data = self.preprocess(data)
      self.sock.sendto(send_data, self.server_address)
      data, addr = self.sock.recvfrom(self.buffer_size)
    except socket.error, msg :
      logging.info('Failed to send/receive: ' + msg)

  def preprocess(self, raw_data):
    data = self.add_timestamp(raw_data)

    try:
      compressed = data.encode("zlib")
      if len(compressed) >= len(data):
        return "0" + data
      else:
        return "1" + data.encode("zlib")
    except:
      logging.info('Failed to preprocess data')
      return data

  def close(self):
    try:
      self.sock.close()
    except:
      logging.info('Failed to close socket')

  def add_timestamp(self, data):
    return datetime.datetime.now().strftime("%b %d %H:%M:%S") + " " + str(data)
