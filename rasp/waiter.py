#! /usr/bin/python
# -*- coding: utf-8 -*-

import logging
import socket
from struct import *

class Waiter:
  upd_port = 514
  buffer_size = 2048
  debug = True

  def __init__(self):
    try:
      self.sock = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_UDP)
    except socket.error, msg:
      logging.info('Failed to create waiter socket: ' + msg)
      self.sock = None

    self.reset()

  def valid(self):
    return self.sock != None

  def receive(self):
    self.reset()
    packet = self.sock.recvfrom(self.buffer_size)
    packet = packet[0]

    try:
      # IP header.
      ip_header_packet = packet[0:20]
      ip_header = unpack('!BBHHHBBH4s4s', ip_header_packet)

      # Version.
      version_ihl = ip_header[0]
      version = version_ihl >> 4
      ihl = version_ihl & 0xF
      iph_length = ihl * 4

      # TTL.
      ttl = ip_header[5]

      # Protocol.
      protocol = ip_header[6]

      # Source and Destination addresses.
      source_addr = socket.inet_ntoa(ip_header[8]);
      destination_addr = socket.inet_ntoa(ip_header[9]);

      # UDP header packet.
      udp_header_packet = packet[iph_length:iph_length + 8]
      udp_header = unpack('>hhhh' , udp_header_packet)

      # Source and destination port.
      source_port = udp_header[0]
      dest_port = udp_header[1]
      #length = udp_header[2]

      # Only capture port from rasberry.
      if dest_port == self.upd_port:
        header_size = iph_length + 2 * 4
        data_size = len(packet) - header_size
        self.received_data = packet[header_size:]

        if self.debug:
          logging.info(source_port + "->" + dest_port + ":" + str(self.received_data))
    except:
      logging.info('Something wrong when parsing UDP package')

  def reset(self):
    self.received_data = None

  def close(self):
    self.sock.close()

