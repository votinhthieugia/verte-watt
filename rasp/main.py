#! /usr/bin/python
# -*- coding: utf-8 -*-

import logging
import sys

from time import time
from waiter import Waiter
from raspclient import RaspClient

# Config logging.
logging.basicConfig(filename='gclg.log',level=logging.DEBUG)

# Server host and port to send captured data.
server_host = 'localhost'
server_port = 8000
sleep_time = 180
active_time = 60
keep_active = True

# Initialize waiter and client sockets.
waiter = Waiter()
client = RaspClient(server_host, server_port)

# If initiation failed, quit.
if not waiter.valid() or not client.valid():
  sys.exit()

# Loop to receive data from injection server.
start_time = 0

while True:
  # If in active time.
  if keep_active or start_time == 0 or time() - start_time <= active_time:
    waiter.receive()

    if waiter.received_data:
      client.send(waiter.received_data)
      start_time = time()

  # TODO: Do we need this??
  # Sleep if possible.
  if not keep_active and (start_time > 0 && time() - start_time > active_time):
    time.sleep(sleep_time)


# Close all sockets.
client.close()
waiter.close()
