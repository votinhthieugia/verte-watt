#! /usr/bin/python
# -*- coding: utf-8 -*-

import logging
import socket

from store import Store

class RaspServer:
  # TODO: Should increase this when neccesary??
  buffer_size = 1024

  def __init__(self, host, port):
    server_address = ('', port)

    try:
      self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    except socket.error, msg : 
      self.sock = None
      logging.info('Failed to create socket: ' + msg)

    try:
      self.sock.bind(server_address)
      logging.info('Socket bind complete')
    except socket.error, msg : 
      self.sock = None
      logging.info('Failed to bind socket: ' + msg)

    self.writer = Store()

  def valid(self):
    return self.sock != None

  def receive(self):
    data, addr = self.sock.recvfrom(self.buffer_size)

    if data:
      # Reply.
      self.sock.sendto('', addr)

      # Write data to file.
      self.writer.write(data)


  def close(self):
    self.sock.close()
