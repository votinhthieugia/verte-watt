#! /usr/bin/python
# -*- coding: utf-8 -*-

import logging
import os
import time

class Store:
  file_path = '/opt/gclc/gclc.log'
  #file_path = './test.log'
  debug = True
  last_time = time.time()
  interval = 15 * 60

  def __init__(self):
    try:
      self.file = open(self.file_path, "a")
      self.last_time = time.time()
    except:
      logging.info('Failed to open file')

  def reopen(self):
    try:
      self.close()
      os.remove(file_path)
      self.file = open(self.file_path, "a")
      self.last_time = time.time()
    except:
      logging.info('Failed to reopen file')

  def preprocess(self, raw_data):
    is_not_encoded = str(raw_data[0]) == "0"
    real_data = raw_data[1:]
    if is_not_encoded:
      return real_data
    else:
      return real_data.decode("zlib")

  def write(self, raw_data):
    if time.time() - self.last_time >= self.interval:
      self.reopen()

    data = preprocess(raw_data)

    if self.debug:
      logging.info('Write data to file:' + str(data))

    if self.file:
      try:
        self.file.write(data)
        self.file.flush()
      except:
        logging.info('Failed to write data to file')

  def close(self):
    if self.file:
      self.file.close()


