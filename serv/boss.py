#! /usr/bin/python
# -*- coding: utf-8 -*-

import logging
import sys

from raspserver import RaspServer

# Config logging.
logging.basicConfig(filename='gclg.log',level=logging.DEBUG)

# Init the socket server.
server = RaspServer('', 8000)

# Quit if something wrong.
if not server.valid():
  sys.exit()

# Loop to receive data from raspberry client.
while True:
  server.receive()

# Close the server.
server.close()
